angular
.module('TransactionsModule')
.service('transactionService',[ '$http', '$timeout', function($http, $timeout){
    let service = this;
    service.transactions = [];

    service.getAllTransactions = async function() {
        if (service.transactions.length === 0 ) {
          service.transactions = await $http.get('/../home/data/transactions.json')
            .then(function(resp){
              return resp.data;
            });
        }
  
        return service.transactions;
    }

    // send transactions from transfer form
    service.sendTransaction = function (transaction) {
      service.transactions.push(transaction);
    }

    // fetch transaction from 'backend'
    service.getTransaction = function(id) {
        function transactionMatchesParam(element) {
            return parseInt(element.id) === parseInt(id);
        }
  
        return service.getAllTransactions()
        .then(transactions => {
          return transactions.find(transactionMatchesParam);
        })
      }
    
  }]);