angular
.module('TransactionsModule')
.component('transactionDetailComponent', {
    bindings: { 
      transaction : '<'
    } ,
    templateUrl: "home/components/transactions/transaction-detail/transaction-detail.html",
    controller: "transactionDetailController"
  })
.config(function($stateProvider) {
    $stateProvider
    .state(
        'home.transaction-detail', {
          url: "/transaction/{id}",
          component: 'transactionDetailComponent',
          resolve: {
            transaction: async function(transactionService, $transition$) {
              return await transactionService.getTransaction($transition$.params().id);
            }
          }
        })
})