angular
  .module('TransactionsModule')
  .controller('transactionsListController', ['transactionService', '$scope', '$timeout', '$state', function (transactionService, $scope, $timeout, $state) {
    ctrl = this;
    console.log('transactionsListController reached');
  
    transactionService.getAllTransactions().then(transactions => {
      $timeout(() => { ctrl.transactions = transactions; }, 0)
    });

   
  
    $scope.reverse = true;
    $scope.sortBy = function(propertyName) {
        $scope.reverse = (propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

  }]);

