angular
.module('TransactionsModule')
.component('transactionsListComponent', {
    bindings: { transactions: '<' },
    templateUrl: "home/components/transactions/transactions-list/transactions-list.html",
    controller: 'transactionsListController'
  })