class HomeController {
    constructor (AuthenticationService, $firebaseAuth, firebase, $scope) {
        this.AuthenticationService = AuthenticationService;
        this.$firebaseAuth = $firebaseAuth;
        this.firebase = firebase;
        this.$scope = $scope;

        this.init();
    }
    
    init () {
           console.log('Home Controller reached')
           this.$scope.user = this.AuthenticationService.getUser();
         
           // reset login status
           this.$scope.logout =  () => {
               this.AuthenticationService.logout();
           };
   
       }
   
}


angular
    .module('peachtreeApp')
    .controller('HomeController', HomeController)