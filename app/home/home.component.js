angular
    .module('peachtreeApp')
    .component('home', {
        templateUrl: 'home/home.html',
        controller: 'HomeController'
    });