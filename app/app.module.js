angular
  .module('peachtreeApp', ['ui.router', 'ngCookies', 'Authentication', 'TransactionsModule'])


  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    console.log('app.js - config');
    $stateProvider
      .state("login", {
        url: "/login",
        templateUrl: "auth/login/login.html",
        controller: "LoginController",
        data: {
          requiredAuth: false
        },
      })

      .state("register", {
        url: "/register",
        templateUrl: "auth/register/register.html",
        controller: "RegisterController", 
        data: {
          requiredAuth: false
        },
      })

      .state("home", {
        templateUrl: "home/home.html",
        controller: "HomeController",
        data: {
          requiredAuth: true
        },
      })

      .state(
        'home.transactions-list',{
        url:'/transactions',
        component: 'transactionsListComponent'
        })

    $urlRouterProvider.otherwise('/login');
  }
  ])

  .run(function ($transitions, $state, AuthenticationService) {
    $transitions.onStart({
      to: 'login'
    }, function () {
      let currentUser = firebase.auth().currentUser;
      if (currentUser) {
        console.log('currentuser')
        return $state.go('home')
      } else {
        console.log('no currentuser')
        return $state.go('login');
      }
    });
  });
    // $transitions.onStart({
    //   to: 'login' 
    // }, function () {
    //   if (AuthenticationService.isAuthenticated()) {
    //      $state.go('home');
    //   }
    // })
  // })
